#!/bin/sh
set -e

# make sure rsyslog is running
service rsyslog start

# make sure cron is running
service cron start

# pass off to parent entrypoint
exec /docker-entrypoint.sh "$@"
