# HAProxy + RSYSLOG + Logrotate #

This docker repo is built to provide an HAProxy image with logging via RSYSLOG, configured with a 30 daily logrotates.

Built off of the official [HAProxy Docker image](https://hub.docker.com/_/haproxy).
