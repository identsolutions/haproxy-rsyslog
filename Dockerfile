FROM haproxy:1.8.25

RUN apt-get update && apt-get -y install rsyslog vim && rm -rf /var/lib/apt/lists/* && touch /var/lib/logrotate/status

COPY haproxy.logrotate /etc/logrotate.d/haproxy
COPY haproxy.rsyslog /etc/rsyslog.d/haproxy.conf
COPY haproxy-rsyslog-entrypoint.sh /

ENTRYPOINT ["/haproxy-rsyslog-entrypoint.sh"]
CMD ["haproxy", "-f", "/usr/local/etc/haproxy/haproxy.cfg"]
